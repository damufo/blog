<!--
.. title: About
.. slug: about
.. date: 2020-05-17 09:07:52 UTC+02:00
.. tags:
.. category:
.. link:
.. description: Who am I?
.. type: text
-->

<img id="face-photo" src="/images/about.jpg" alt="Radek Sprta" class="pull-right is-rounded">

Hey, there. My name is Radek Sprta. I am a Linux admin with focus on DevOps and openly in love with Python. I help running some the largest e-commerce sites in Czechia at [vshosting](https://vshosting.cz/references). I spend the majority of my time there automating stuff. In the off hours, I contribute to several open-source projects. You can check them on my [Github](https://github.com/radek-sprta) and [Gitlab](https://gitlab.com/radek-sprta) pages.

Ever since I spent a year living in Kyoto, my biggest passion outside IT has been Japanese. I did gigs as translator, interpreter and even worked on a [book](https://www.lingea.cz/japonska-gramatika.html) about it.

Connect with me on one of the links in the navigation.

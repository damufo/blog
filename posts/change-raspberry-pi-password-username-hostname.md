<!--
.. title: Change Raspberry Pi Password, Username and Hostname
.. slug: change-raspberry-pi-password-username-hostname
.. date: 2017-06-26 15:30:58
.. tags: security,server,setup,tutorial
.. category: Raspberry Pi
.. link:
.. description:
.. type: text
.. categories: Raspberry Pi
.. has_math: no
.. status: published
.. wp-status: publish
-->

Rapsberry Pi is the single most popular computer in the world. And it is
easily one of the most hacked systems as well. That's due to single fact -
most users never change the default username and password. But is a relatively
a simple process, so there is no reason not to do it. If you already have your
Raspberry Pi [up and running](https://radeksprta.eu/setting-up-headless-raspberry-pi-server/), you can get going.
<!-- TEASER_END -->

## Changing the hostname

First of all, change the hostname. This has two benefits: it will make it
slightly harder for hackers to identify your Pi, and it will also any prevent
any clashes on network if you have multiple Raspberries. So, fire up `raspi-
config`:

```text
$ sudo raspi-config
```

And choose the option "Change the Hostname". Then type in
the new hostname (for example fileserver, webserver) and confirm it. Done.

## Change the username and password

Next on the chopping block is username. Since renaming user is a bit of a
hassle, just create a new one and lock the `pi` account. (Substitute `newuser`
for your desired username.)

```bash
$ sudo useradd newuser -m -g pi
$ for GROUP in $(groups pi | sed 's/.*:\s//');
  do sudo adduser newuser $GROUP;
done
```

The new user should be set up and
have the same priviledges as `pi`. All that remains is changing the password:

```text
$ sudo passwd dak
Enter new UNIX password:
Retype new UNIX password:
passwd: password updated successfully
```

Finally, login to the new user account to check if
everything is working as expected:

```text
$ su newuser
```

If you don't encounter any
trouble, go ahead and lock the `pi` user:

```text
$ sudo passwd -l pi
```

Hostname, username,
password - everything is changed. Now you can sleep tight, knowing that the
hackers cannot simply login to your Pi as if it were their own.

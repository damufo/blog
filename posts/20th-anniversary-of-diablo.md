<!--
.. title: 20th Anniversary of Diablo
.. slug: 20th-anniversary-of-diablo
.. date: 2016-11-06 21:15:57
.. tags: anniversary,blizzard,diablo,remake,rpg
.. category: Games
.. link: 
.. description: 
.. type: text
.. categories: Games
.. has_math: no
.. status: published
.. wp-status: publish
-->

Diablo was my favorite game as a kid. I remember the captivating gloomy,
haunted atmosphere, that kept you incessantly on your toes, as if it were
yesterday. It is hard to believe that twenty years have come by.  Yesterday,
Blizzard have announced that they have [something special in store for the
20th Diablo anniversary](https://us.battle.net/d3/en/blog/20352105/blizzcon-recap-day-1-11-4-2016).
In the upcoming patch of Diablo 3, they will have
recreated the original Diablo campaign. And to make the experience as
authentic as possible, characters will only be able to move in 8 directions
(instead of 360 degrees). Also, a special grainy filter will be applied to
graphics.

![Original Diablo remade in Diablo
3](https://bnetcmsus-a.akamaihd.net/cms/gallery/JVNM0W7UZ5TJ1478312107315.jpg)

Perhaps it is time to finally buy Diablo 3?

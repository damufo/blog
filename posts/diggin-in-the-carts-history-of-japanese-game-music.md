<!--
.. title: Diggin' in the Carts - History of Japanese Game Music
.. slug: diggin-in-the-carts-history-of-japanese-game-music
.. date: 2016-12-12 10:20:25
.. tags: 8 bit,consoles,documentary,music,ost,Link
.. category: Games
.. link: 
.. description: 
.. type: text
.. categories: Games,Japanese
.. has_math: no
.. status: published
.. wp-status: publish
-->

As a kid, I spent countless hours playing 8-bit games. Atari, Nintendo, Sega -
had it all. And 8-bit music has had a special place in my heart ever since.
Even now, I like to pop a song or two from my favorite games. Not just because
of the nostalgia, but because of the special sound qualities of 8-bit, that
you won't find anywhere else. Some of soundtracks are true masterpieces, as
evidenced by their being performed by orchestras all over the world. It is
amazing how the composers could achieve such acoustic richness, given how
little they had to work with.

When I heard from my brother about a Diggin' in
the Carts, a documentary series by Red Bull Music Academy, that covers the
history of Japanese game music, I was overjoyed. It features numerous
interviews with the composers, giving a unique insight into how some of the
most iconic game songs came to light, with a sprinkle of comments by musicians
and electronic producers from around the world, who talk about the impact that
Japanese game music had on their works. If you are a fan of the 8-bit sound or
retro gaming, this is a must-see. As an added bonus, you can turn off the
subtitles to get some quality Japanese practice. You can find the whole series
on the following page:

[Diggin' in the
Carts](https://daily.redbullmusicacademy.com/2014/10/diggin-in-the-carts-series)


<!--
.. title: Automatic ClamAV Scans With Email Notifications
.. slug: automatic-clamav-scans-with-email-notifications
.. date: 2018-09-18 09:42:52
.. tags: antivirus,automatization,clamav,security,tutorial
.. category: Linux
.. link:
.. description:
.. type: text
.. categories: Unsorted
.. has_math: no
.. status: published
.. wp-status: publish
-->

You might consider antivirus software unneccessary on a Linux box. But if you
are running a public facing server, it might be a good idea to run it
regardless. Why is that? Even though malware probably can't affect your server
much, it can still use it to infect users. To prevent that, we will learn how
to install and set up automatic [ClamAV](https://www.clamav.net/) scans with
email notifications. In the examples below, I am using Debian 9, but the
commands should be the same for any Linux distribution.

## Install and configure ClamAV

You can easily install the antivirus by running the following command:
```text
$ apt install clamav clamscan
```

ClamAV is pretty well
configured out of the box. Usually, I only change the frequency of virus
signature database updates. You can do that in `/etc/clamav/freshclam.conf` by
changing the `Checks` line. Since I run the scan once a day, I lower the
frequency of updates to that as well.

```text
Checks 1
```
<!-- TEASER_END -->

## Schedule automatic ClamAV scans

The easiest way to schedule the checks is cron, since it is readily available
on all major distributions. To add a cron job, run:

```text
$ crontab -e
```
This will open the root's crontab in you favorite
editor. Then add the following lines at the end:

```bash
MAILTO=admin@example.org
03 3 * * * /usr/bin/clamscan -ri --no- summary /
```

The first line specifies an email address, where the
reports should be sent to. The second one is the cron job itself. In this
case, it's set to run at 3:30 every morning. Now, let me quickly go through
the clamscan options that I use:

  1. `-r` scans directory recursively
  2. `-i` prints only infected files
  3. `--no-summary`, as the name suggests, doesn't display the summary at the end of the scan

The last argument is the directory to scan. In the example above, the whole
file system. With this setup, clamscan will run every morning, but only send
notification emails if it finds any infected files. To increase your servers'
security further, learn how to [setup password-less
SSH](https://radeksprta.eu/set-up-password-less-ssh-login-and-secure-your-ssh/).


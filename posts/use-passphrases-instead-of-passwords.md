<!--
.. title: Use passphrases instead of passwords
.. slug: use-passphrases-instead-of-passwords
.. date: 2019-08-21 18:41:39
.. tags: passphrase,password,privacy,security
.. category: Essay
.. link: 
.. description: 
.. type: text
.. categories: Essay
.. has_math: no
.. status: published
.. wp-status: publish
-->

The other day I wanted to buy a concert ticket from a website I haven't
visited before. That meant creating a new account (even though that shouldn't
really be necessary). I went through the ordeal of filling in my details,
clicked "Confirm" and *bam*… the password was not good enough. As is usual,
the password needed to contain an uppercase letter, a numeral, a symbol, a
Chinese character and 10 emojis (couldn't you have told me earlier?). At this
point, most people would just say screw it and use a variation of one their
few passwords. Perhaps adding their birthdate or something along the lines. I
draw that conclusion from publicly available lists of breached passwords. To
be honest, I don't blame them. Remembering tens of passwords is hard, even
without all the weird characters. But even if you don't feel like setting up a
password manager, there is a better and safer alternative. Passphrases.

## What are passphrases?

As you might have guessed, passphrases use several words, or a sentence,
instead of a single word. Nowadays, you can readily use spaces in password.
That's why I would recommend using whole sentences. That typically makes your
password much longer. And as a bonus, using commas etc., you will easily
fulfill the special character criteria. However, it is important to choose a
unique phrase, not something you would easily find on the Internet. While it
might be tempting to just use "Winter is coming to Winterfell", that's about
as good as "12345". I recommend using something based on a memory.
<!-- TEASER_END -->

## Passphrase complexity

How difficult are passphrases to crack though? At the first glimpse, it might
seem that traditional passwords are more complex and therefore must be harder
to crack. But length by itself adds complexity. A 25 character passphrase
(about the length of an average sentence) would take billions of years to
crack with the current technology. Unless somebody has secret quantum
supercomputer in their basement, they won't be doing that anytime soon. On the
other hand, an 8 character password, made up of all sorts of gibberish, would
take days to figure out at most.

## Conclusion

As you can see, passphrases are both safer and easier to remember. Over the
years, we were trained to use hard-to-remember and easy-to-crack passwords. It
is time to stop that. And if you don't think you stand much to lose by having
your password cracked, [think again](https://radeksprta.eu/why-privacy-matters/).

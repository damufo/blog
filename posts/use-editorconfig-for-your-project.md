<!--
.. title: Use EditorConfig For Your Project
.. slug: use-editorconfig-for-your-project
.. date: 2018-07-06 11:01:57
.. tags: configuration,programming
.. category: Linux
.. link:
.. description:
.. type: text
.. categories: Unsorted
.. has_math: no
.. status: published
.. wp-status: publish
-->

This might be the first time you're hearing about EditorConfig. But don't
worry, I was in the same boat just a couple of months ago. So, what is it? It
is a simple file format, that helps keeping coding styles consistent across
different editors. And more importantly, across different team members.

## How does it work?

EditorConfig consists of two components. Firstly, .editorconfig file, that you
put in the root of your project. Secondly, a plugin for the editor of your
choice.  The .editorconfig file is written in .ini format. You can define
things such as number of spaces to indent or whether to use tabs or spaces.
Moreover, you can use different settings for different file formats. For
complete explanation, head over to the [official
site](https://editorconfig.org/#file-format-details). In order to enforce the
settings from .editorconfig, you need a plugin for your editor. For the most
popular ones, such as Vim, Emacs, Atom, Sublime Text or Brackets, the are
plugins readily available. The Jetbrains IDEs even include EditorConfig
support in their core.
<!-- TEASER_END -->

## EditorConfig Example

To give you a better idea of EditorConfig, I decided to include a
configuration that I use for one of my projects. As you can see, it is fairly
short:

```ini
# Top-most EditorConfig file.
root = true
# Set charset and Unix-style newlines
[*]
charset = utf-8
end_of_line = lf
insert_final_newline = true
trim_trailing_whitespace = true

[*.{py,ini}]
indent_style = space
indent_size = 4

[*.{yml,yaml}]
indent_style = space
indent_size = 2
```

## Conclusion

With everything set up, you can enjoy the benefits of not having to configure
your editors manually for every project. To save even more time, see my advice
on how to [manage your Vim plugins](https://radeksprta.eu/managing-vim-plugins-easily-using-git/)
and [increase your productivity](https://radeksprta.eu/vim-tips-increased-productivity/).

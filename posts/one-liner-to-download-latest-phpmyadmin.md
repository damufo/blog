<!--
.. title: One-liner to Download Latest PHPMyAdmin
.. slug: one-liner-to-download-latest-phpmyadmin
.. date: 2020-07-01 16:49:55 UTC+02:00
.. tags: phpmyadmin, automatization, one-liner, bash
.. category: DevOps
.. link: 
.. description: PHPMyAdmin does not have any direct link to the latest version, making it 
hard to maintain. Get it using this bash one-liner.
.. type: text
-->

Even though the PHPMyAdmin's heyday is long gone by, it still remains quite popular. However,
unless you are running it as a Docker container or similar, it is difficult to maintain updated.
The reason being, that the creators do not offer any direct link to get the latest version. And
the version in system repositories is usually few releases behind.

Luckily, their website design has remained fairly stale, so you can scrape the download link
from there. The bash one-liner below (split into three lines for readability :)) will achieve
just that:

```bash
curl https://www.phpmyadmin.net/files/ 2>/dev/null \
  | grep -oP '(?<=href=")https://files.phpmyadmin.net/phpMyAdmin[^"]*(?=")' \
  | head -n 1
```

It uses the nifty look-ahead feature (`(?<=)`) of the Perl-like expression matching. But that
is specific to GNU Grep, so you might need to install it, if you are on BSD or MacOS.

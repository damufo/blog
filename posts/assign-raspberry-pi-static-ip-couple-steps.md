<!--
.. title: Assign Your Raspberry Pi Static IP in Couple Steps
.. slug: assign-raspberry-pi-static-ip-couple-steps
.. date: 2017-10-28 06:23:57
.. tags: configuration,internet,network,server,static ip
.. category: Raspberry Pi
.. link: 
.. description: 
.. type: text
.. categories: Raspberry Pi
.. has_math: no
.. status: published
.. wp-status: publish
-->

Due to its availability, Raspberry Pi is an ideal candidate to run a simple
server. After the [initial setup](https://radeksprta.eu/setting-up-headless-raspberry-pi-server/),
it will use DHCP to connect to your network. That
means, each time you restart the Pi or your router, it might end up with a
different IP address. This not ideal, as you will need to take a note of the
new IP to connect to your services. In order to avoid reconfiguring your
computers all the time, you should assign your Raspberry Pi static IP. This
post will show you how to do it for ethernet connection on Raspbian, but the
steps are the same for a [wireless connection](https://radeksprta.eu/raspberry-pi-wifi-setup/).
Start by connecting to your Pi. When you do, you can see the
current network settings by running: 

```text
$ cat /etc/network/interfaces
```

It should look
something like this: 

```text
# interfaces(5) file used by ifup(8) and ifdown(8)
auto lo 
iface lo inet loopback
iface eth0 inet dhcp
```

The entry you need to change is for the `eth0` interface. That is the default
ethernet adapter. In order to use a static IP, you have to provide IP address,
network, network mask, broadcast and default gateway. So, let's get them.
<!-- TEASER_END -->

## Find network settings

First, use the ip command to get your IP address: 

```text
$ ip a
1: lo:  mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
  inet 127.0.0.1/8 scope host lo 
    valid_lft forever preferred_lft forever
  inet6
    ::1/128 scope host valid_lft forever preferred_lft forever
2: eth0:  mtu 1500 qdisc pfifo_fast state DOWN group default qlen 1000
      link/ether 84:34:97:7e:61:00 brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.100/24 brd 192.168.0.255 scope global eth0
      valid_lft forever preferred_lft forever
    inet6 :belllbelb/128 scope host
      valid_lft forever preferred_lft forever
```

In the `eth0` section, after `inet`, you can find the IP address. Write it
down. Next to it, after `brd`, is the broadcast. For the remaining the values,
run: 

```text
$ ip r
default via 192.168.0.1
dev eth0 192.168.0.0/24 dev eth0 proto kernel scope link src 192.168.0.100
```

Default shows the default gateway. Just under it you
can see the network (192.168.0.0). As for the mask, common network masks are
/24, which corresponds to 255.255.255.0, and /16, which stands for
255.255.0.0. These are all things that you need, so onto the next part.

## Assign Raspberry PI Static IP

Now, let's go back to the `/etc/network/interfaces`. It is time to put in the
new values. Change eth0 entry so it looks like this:

```text
iface eth0 inet static
    address 192.168.0.100/24
    gateway 192.168.0.1
```

Go ahead and save the file.
Lastly, delete the dhcp leases and reboot you Raspberry. 

```bash
$ sudo rm /var/lib/dhcp/dhcpclient.leases?
$ sudo reboot now
```

After it boots up, check that the connection is running
by pinging the gateway: 

```text
$ ping 192.168.0.1
```

## Conclusion

Congratulations! You have successfully assigned a static IP address to your
Raspberry Pi. From now on, you can connect to your services without having to
worry about the ever-changing IP. If you have any questions, write them in the
comments below.

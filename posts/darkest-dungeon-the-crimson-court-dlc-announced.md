<!--
.. title: Darkest Dungeon - The Crimson Court DLC Announced
.. slug: darkest-dungeon-the-crimson-court-dlc-announced
.. date: 2016-10-05 13:42:24
.. tags: darkest dungeon,dlc,rpg
.. category: Games
.. link: 
.. description: 
.. type: text
.. categories: Games
.. has_math: no
.. status: published
.. wp-status: publish
-->

![Darkest Dungeon - The Crimson Court
poster](https://hb.imgix.net/f2eedf836037a563276c7e18f976df748405f7f1.jpg?auto=compress,format&fit=crop&h=353&w=616&s=3174134eb406b8752d88d04028303bcb)

Just days after the [release of
Darkest Dungeon for PS4 and PS Vita](https://radeksprta.eu/posts/darkest-dungeon-new-czech-translation/),
Red Hook studios have announced a
new DLC called "The Crimson Court".  Truthfully, the official announcement
does not say much except the release date - early 2017. And that the DLC will
focus on vampires. As is Red Hook's tradition, they will probably not be your
typical vampires. But that still leaves a plenty of room for speculation. So,
my guess is we can expect a new dungeon. Perhaps also some new classes - a
vampire hunter, vampire or even a
[dhampir](https://en.wikipedia.org/wiki/Dhampir). What is it going o be? Let's
see! I feel excited already.


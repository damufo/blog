<!--
.. title: On Seeing the 100% Perfect Girl One Beautiful April Morning
.. slug: on-seeing-the-100-perfect-girl-one-beautiful-april-morning
.. date: 2014-12-23 11:30:39
.. tags: Murakami,translation
.. category: Japanese
.. link: 
.. description: 
.. type: text
.. categories: Japanese
.. has_math: no
.. status: published
.. wp-status: publish
-->

On Seeing the 100% Perfect Girl One Beautiful April Morning is one of my
favorite Murakami's works and probably my favorite short story overall. I
wholeheartedly recommend everybody to read it. Unfortunately, it was not
published in Czech yet. Nor does it seem it will be anytime soon, so I have
decided to translate it myself.

## Jak jsem jednoho krásného dubnového rána potkal 100% dokonalou dívku

Jednoho krásného dubnového rána jsem v uličce na čtvrti Haradžuku prošel okolo
100% dokonalé dívky. Upřímně řečeno, nebyla zas tak pěkná. Nebo že by na ní
něco poutalo pozornost. Ani na sobě neměla nijak skvělé šaty. Vlasy měla vzadu
ještě po ránu rozcuchané a co se věku týče, nebyla už úplně mladá. Mohlo jí
být tak ke třicíti. Technicky vzato, už to vlastně ani nebyla dívka. Bez
ohledu na to, už z 50 metrů mi to bylo jasné. Ona je pro mě 100% dokonalou
dívkou. V okamžiku, kdy jsem ji spatřil, se mi srdce rozbušilo jako o závod a
v ústech mi vyprahlo jako na poušti.

Možná máte mezi holkami nějaký svůj typ.
Třeba se vám líbí holky se štíhlými kotníky, nebo velkýma očima, potrpíte si
na pěkné prsty nebo vás z nějakého důvodu přitahují dívky, co si pomalu
vychutnávají jídlo. Já mám samozřejmě taky svůj typ. Někdy když jsem v
restauraci, okouzleně pozoruju tvar nosu dívky u sousedního stolu. Ale popsat
dokonalou dívku, to nikdo nedokáže. Jaký měla nos, to si vůbec nedokážu
vybavit. Vlastně si ani dost dobře nepamatuju, jestli nějaký měla. Jediné co
si vzpomínám, že to nebyla kdovíjaká kráska. Je to zvláštní.
<!-- TEASER_END -->

"Včera jsem potkal 100% dokonalou holku," říkám komusi.  
"Hm," odpověděl. "Byla to kočka?"  
"No, ani ne."  
"Takže to byl tvůj typ, co?"  
"To si právě nevzpomínám. Jaký měla tvar očí, jestli měla velký nebo malý prsa, skoro nic  
si nepamatuju."  
"To je teda divný, ne?"  
"Fakt divný."  
"Mno a," řekl znuděně, "udělals něco? Oslovil ji, šel za ní nebo tak?"  
"Vůbec nic," odvětil jsem. "Jen jsme okolo sebe prošli."  

Ona šla od východu na západ, já od západu na východ. Bylo to velice příjemné
dubnové ráno. Říkám si, že bych si s ní rád popovídal, kdyby jen na půl
hodiny. Chtěl bych se o ní dozvědět víc a taky jí povědět o sobě. A ze všeho
nejvíc bych chtěl zjistit, jaké cestičky osudu vedly k tomu, že jsme se
potkali tohohle krásného dubového rána na uličce v Haradžuku. Určitě jsou plná
tajemství, vřelých jako nějaký starobylý stroj z dob kdy zemi vládl mír. Až
bychom si tohle všechno řekli, dali bychom si společně oběd, podívali se třeba
na film od Woodyho Allena a šli si dát do hotelového baru koktejl nebo něco.
Kdyby všechno šlo hladce, možná bychom se pak spolu i vyspali. Příležitost mi
klepe na srdce. Už nás dělí pouhých patnáct metrů. Ale jak ji mám k čertu
oslovit?

"Dobrý den, můžete si na mě udělat půl hodinky čas?"

To je směšné.  Jako bych jí chtěl nabídnout pojištění.

"Promiňte, nevíte jestli je tu někde nonstop čistírna?"

To je asi stejně tak hloupé. Tak za prvé, nemám ani žádnou tašku s prádlem. Kdo by něčemu takovému uvěřil?


Nebo by možná bylo lepší být upřímný a jít přímo k věci.

"Dobrý den, vy jste pro mě 100% dokonalá dívka."

To ne, tomu by nejspíš neuvěřila. A i kdyby nakrásně ano, tak by se se mnou
možná nechtěla bavit. Řekla by něco jako: "Já jsem pro Vás možná 100% dokonalá
dívka, ale Vy pro mě nejste 100% dokonalý muž. Je mi líto." To je dost dobře
možné. A kdyby to takhle dopadlo, dozajista bych se úplně zarazil. Z takového
šoku už bych se asi nezotavil. Je mi už dvaatřicet a to se holt s přibývajícím
věkem stává.

Před květinářstvím jsme se minuli. Na kůži ucítím závan teplého
vzduchu. Asfaltová vozovka je pokropená vodou a okolo voní růže. Ani se
nezmůžu na ni promluvit. Má na sobě bílý svetr a v pravé ruce nese bílou
obálku, ještě bez známky. Napsala někomu dopis. Podle toho jak hrozně ospale
vypadá, strávila jeho psaním snad celou noc. A v té hranaté obálce jsou dost
možná zapečetěná všechna její tajemství. Když jsem se po pár krocích ohlédl,
její silueta už zmizela v davu.

Teď už samozřejmě dobře vím, jak jsem ji tehdy měl oslovit. Ale něco tak
dlouhého by se mi stejně nepodařilo dobře podat. Věci nikdy nejdou podle mých
představ. Každopádně, začal bych "Bylo nebylo" a zakončil to "Není to ale
smutný příběh?"

Bylo nebylo, žili jeden chlapec a dívka. Chlapci bylo osmnáct,
dívce šestnáct let. Nebyl to nijak zvlášť hezký chlapec ani nijak zvlášť
překrásná dívka. Takový obyčejný osamělý chlapec s dívkou, jaké byste našli
kdekoliv. Ale oni pevně věřili, že někde ve světě pro ně existuje 100%
dokonalý protějšek. Ano, věřili v zázrak. A ten se skutečně stal. Jednoho dne
na sebe zčistajasna narazili na rohu ulice.

"To jsem blázen, tebe jsem celou dobu hledal. Možná mi to nebudeš věřit, ale ty jsi pro mě 100% dokonalá holka," řekl jí chlapec.  
"A ty pro mě zas dokonalý kluk. Přesně jako jsem si tě představovala. To je úplně jako ve snu," odvětila mu dívka.  

Sedli si do parku na lavičku, vzali se za ruce a bez přestání si povídali. Už nebyli sami.
Našli svůj 100% dokonalý protějšek a ten si našel je. A něco takového, to je
přece úžasná věc. To je přímo dílo nebeských sil. Ale v srdcích jim vrtala
maličká, skutečně malinkatá pochybnost. Je v pořádku dosáhnout svého snu
takhle snadno?

Když jejich konverzace najednou utichla, chlapec povídá: "Hele,
pojďme to vyzkoušet ještě jednou. Pokud jsme si opravdu souzení, tak na sebe
určitě zase někde narazíme. A až se příště potkáme, bude jasné, že jsme pro
sebe opravdu stvořeni a na místě se vezmeme. Co ty na to?"  
"Dobře," řekla dívka.

Popravdě řečeno, vůbec nebylo potřeba aby tohle zkoušeli. Něco takového
prostě nemuseli dělat. Byli totiž skutečně 100% dokonalým párem. To je samo o
sobě zázrak. Ale protože byli příliš mladí, něco takového nemohli vědet. A jak
už to tak bývá, osud si s nimi krutě pohrál. Jedné zimy se nakazili těžkou
chřipkou, která tehdy řádila, a po několika týdnech, kdy se potáceli na
pokraji života a smrti, dočista ztratili paměť. Jak to říct? Zkrátka když
znova procitli, v hlavě jim zela prázdnota jako v kasičce mladého D. H.
Lawrence. Ale protože to byli inteligentní a vytrvalí mladí lidé, s velkou
pílí nabyli nových vědomostí a citů a dokázali se úspěšně vrátit do
společnosti. Panečku, byli to ale spořádaní lidé. Zvládli i přestupy na metru
a poslat spěšný dopis. A zažili také 75%, respektive 85% lásku.

Čas uplynul jako voda a chlapci najednou bylo třicet dva a dívce třicet. A jednoho
krásného dubnového rána, když chlapec mířil ulicí na Haradžuku ze západu na
východ, aby si dal ranní kávu, dívka šla téže ulicí z východu na západ, aby si
koupila známku pro spěšné psaní. Uprostřed ulice se setkali. Záblesk
ztracených vzpomínek na okamžik rozjasnil jejich srdce, která se jim
rozbušila. Oba už věděli. Ona je pro mě 100% dokonalou ženou. On je pro mě
100% dokonalým mužem. Ale záblesk jejich vzpomínek byl příliš slabý a slova,
která si před 14 lety řekli, už nebyla tak zřetelná jako tehdy. Bez povšimnutí
kolem sebe prošli a aniž by se ohlédli, zmizeli v davu. Navěky. Není to ale
smutný příběh?

Jo, takhle jsem ji tehdy měl oslovit.

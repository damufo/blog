<!--
.. title: Why Privacy Matters by Glenn Greenwald
.. slug: why-privacy-matters
.. date: 2017-12-11 20:28:03
.. tags: Video,privacy,surveillance,talk,ted
.. category: Essay
.. link: 
.. description: 
.. type: text
.. categories: Essay
.. has_math: no
.. status: published
.. wp-status: publish
-->

For the last couple years, mass surveillance has been on the rise. Both
governments and private entities alike track our every move online. As a
result, our privacy has increasingly eroded. Under such circumstances, it is
ever more important to fight for our right for it. But the problem is, that
majority of population seems apathetic to this. If you are one of such people,
I recommend watching the following TED talk by Glenn Greenwald. It nicely sums
up why privacy matters.

[Why Privacy Matters by Glenn Greenwald](
https://www.ted.com/talks/glenn_greenwald_why_privacy_matters#t-727124)

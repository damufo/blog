<!--
.. title: Powerfish - an Elegant and Informative Prompt
.. slug: powerfish-an-elegant-and-informative-prompt
.. date: 2017-06-16 22:56:23
.. tags: command line,fish,prompt,script
.. category: Linux
.. link: 
.. description: 
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

Please, all welcome [Powerfish](https://gitlab.com/radek-sprta/powerfish), the
one true [Fish](https://fishshell.com/) prompt to rule them all. I know what
you might be thinking: "Custom shell prompts? I am not that much of a geek." I
was like that too. But then I thought, what if instead of just taking up
space, the prompt could show me the information I need?  Powerfish does just
that and looks good too. No more wasting time typing 'git status', having to
amend commits and similar annoyances. But enough words, just see it in action:

![Powerfish - an elegant Fish prompt](https://gitlab.com/radek-sprta/powerfish/raw/master/prompt.png)

You might have noticed similarities
with [Powerline](https://github.com/powerline/powerline). And you would be
right. When I was looking for a nice Fish prompt (to no avail), Powerline
inspired me to write one instead. To get Powerfish, just head over to the
[project Gitlab page](https://gitlab.com/radek-sprta/powerfish). Not using
Fish shell yet? No need to worry, [check out why it's so
awesome](https://fishshell.com/) and go install it.


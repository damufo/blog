<!--
.. title: Install MiniDLNA to Stream Media
.. slug: install-minidlna-to-stream-media
.. date: 2019-01-01 12:31:31
.. tags: dlna,media,minidlna,streaming,tutorial
.. category: Raspberry Pi
.. link:
.. description:
.. type: text
.. categories: Raspberry Pi
.. has_math: no
.. status: published
.. wp-status: publish
-->

[DLNA](https://en.wikipedia.org/wiki/Digital_Living_Network_Alliance) is a
standard for sharing digital media. It is supported by most modern TVs, phones
and other devices. So, if you have a [Raspberry Pi file
server](https://radeksprta.eu/setting-up-headless-raspberry-pi-server/), you
can easily use it to stream media. To achieve that, we will install MiniDLNA.
This guide assumes you are using Raspbian 9, but the commands should be
similar on different flavors of Linux.
<!-- TEASER_END -->

## Install MiniDLNA and configure it

The installation itself is very straightforward, as it consists of one
command:

```text
$ sudo apt install minidlna
```

 After that, you need to configure MiniDLNA at
`/etc/minidlna.conf`. You should change to following lines:

```text
media_dir=V,/media/external/videos
media_dir=A,/media/external/music
media_dir=P,/media/external/pictures
merge_media_dirs=yes
friendly_name=My Media
inotify=yes
```

 I think, that most of the options are self-
explanatory. The only exception being inotify - it means that MiniDLNA will
check for new files periodically. So, you don't have to do it yourself. In the
above settings, I assumed you have your media in several `/media/external/`
directories. Whatever directories you use, you have to make sure, that their
contents are readable by MiniDLNA. Therefore, they have to be readable by
`minidlna` user. One way, is to add it to the `pi` group:

```text
$ sudo adduser minidlna pi
```

 Lastly, you need
to scan the media collection:

```text
$ minidlna -R
$ sudo systemctl restart minidlna
```

## Conclusion

Now, you can stream media to your TV, bluetooth speakers etc. But be careful,
not all devices may accept all file formats. And if you need more advanced
file manipulation, there is always [Samba](https://radeksprta.eu/turn-raspberry-pi-into-torrentbox/).

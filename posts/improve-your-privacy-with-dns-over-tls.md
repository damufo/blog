<!--
.. title: Improve Your Privacy with DNS Over TLS
.. slug: improve-your-privacy-with-dns-over-tls
.. date: 2018-05-29 22:05:08
.. tags: dns,DNS over TLS,encryption,privacy,stubby,tutorial
.. category: Linux
.. link:
.. description:
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

HTTPS is quickly becoming standard, in no small part thanks to [Let's
Encrypt](https://letsencrypt.org/). HTTPS helps keeping ISPs and other third
parties from spying on users. But there is one more gaping hole, the DNS
protocol. And DNS over TLS aims to plug it.

## What is DNS?

"Wait a sec, what's this DNS stuff?" might be your reaction.
[DNS](https://en.wikipedia.org/wiki/Domain_Name_System), or domain name
system, is the Internet's yellow pages. It translates URLs into IP addresses.
So, whenever you type an address into your browser, the computer sends a
request to a DNS server. In turn, the DNS server tells it which server to
connect to. Most DNS servers use DNSSEC to sign their answers. Thanks to that
you be sure the answer you got is not bogus. Unfortunately, the whole
communication is in plain text and therefore visible to anyone en route. This
is where DNS over TLS comes into picture.
<!-- TEASER_END -->

## What is DNS over TLS

[DNS over TLS](https://tools.ietf.org/html/rfc7858) is a new standard that
leverages TLS to encrypt all DNS requests, using port 853. TLS is a battle-
tested protocol, that is also used by HTTPS, among other things, so you can
trust it. DNS over TLS is now supported by Cloudflare and Google, as well as
some [OpenNIC](https://www.opennic.org/) servers, so you have plenty of
options, depending on which DNS provider you trust.

## Setup DNS over TLS using Stubby

But how do you set it up? If you are on Linux, one option is to use
[Stubby](https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Daemon+-+Stubby).
Many distributions already have it in their repositories. Look for the
`getdns` package. Alternatively, you can [build it from
source](https://github.com/getdnsapi/stubby). When installed, edit your
configuration file (usually found at `/usr/local/etc/stubby/stubby.yml`), so
it looks like this:

```text
dns_transport_list: [GETDNS_TRANSPORT_TLS]
tls_authentication: GETDNS_AUTHENTICATION_REQUIRED
listen_addresses:
  - 127.0.0.1@53
upstream_recursive_servers:
  - address_data: 80.211.208.74
    tls_auth_name: "dns.radeksprta.eu"
    tls_pubkey_pinset:
      - digest: "sha256"
        value: teXpPMydh/upfzX5W9tlx1RWxo649tAIMfVlAqzXPuE=
idle_timeout: 60000 # keep-alive for 1 min, for better performance
limit_outstanding_queries: 10 # try to reduce SERVFAILS
timeout: 12000 # 12 sec, try to reduce SERVFAILS
```
and restart Stubby. As you can see, this example uses [my
OpenNIC DNS server](https://dns.radeksprta.eu/). If you prefer to use
Cloudflare instead, use `1.1.1.1` in `address_data` and `tls_auth_name`
fields. Finally, change your `/etc/resolv.conf` file, so it has the following
line on top:

```text
nameserver 127.0.0.1
```

## Conclusion

Your computer will now encrypt all DNS requests, which should stop your ISP
snooping around. For even more [privacy](https://radeksprta.eu/categories/privacy/),
check out [how to use Pi-Hole](https://radeksprta.eu/posts/use-pi-hole-block-ads-raspberry-pi/).
If you are in doubt why that is necessary, checkout the
excellent [Why Privacy Matters by Glenn Greenwald](https://radeksprta.eu/posts/why-privacy-matters/).

<!--
.. title: Japanese Cloze Card Generation Plugin For Anki
.. slug: japanese-cloze-card-generation-plugin-for-anki
.. date: 2014-08-17 10:47:50
.. tags: anki,language learning
.. category: Japanese
.. link: 
.. description: 
.. type: text
.. categories: Japanese
.. has_math: no
.. status: published
.. wp-status: publish
-->

Studying new vocabulary is only really good if you learn the words in context.
And finding the proper context for multiple words can be quite time-consuming.
So I have decided to quicken the process by writing a small plugin for Anki
([a great piece of SRS software, if you have not heard about it
before](https://www.ankisrs.net "http://www.ankisrs.net")), that pulls example
sentences straight from the Tanaka Corpus. I have been using it for quite some
time. Now, I have finally come around to polish it, so that other people can
benefit from it, too.  So, what does it actually do? The plugin comes with a
custom model, which searches the Tanaka corpus for examples and then generates
cloze cards out of them. (If the you are not using [cloze
deletion](https://en.wikipedia.org/wiki/Cloze_test
"https://en.wikipedia.org/wiki/Cloze_test") already, you really should try it
in some form). It also generates the readings, supports multiple clozes per
sentence and user edits. In theory, it should cover all your needs. Using the
plugin is extremely easy. Just search for a Japanese expression, 

![Searching
a Japanese expression](https://radeksprta.eu/wp-content/uploads/2014/08/anki1.png)

choose an example sentence

![Choosing
example sentence](https://radeksprta.eu/wp-content/uploads/2014/08/anki2.png)

and voilà, here is your card.

![Generated
card](https://radeksprta.eu/wp-content/uploads/2014/08/anki3.png)

For any questions or bug reports, head over
to the [plugin's page](https://launchpad.net/japanese-cloze-examples "
https://launchpad.net/japanese-cloze-examples"). You can download the plugin
from there as well, but I recommend using the built-in Anki downloader - the
code is 247705873. You can find other interesting stuff on my [project
page](https://radeksprta.eu/projects/).

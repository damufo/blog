<!--
.. title: Run Docker Container as Regular User
.. slug: run-docker-container-regular-user
.. date: 2017-10-21 14:21:13
.. tags: containers,docker,privileges,security
.. category: Linux
.. link:
.. description:
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

Docker has become omnipresent in recent years. However, by default, it
requires root privileges to run containers. Obviously, this is not good
security-wise. But a simple change will allow you to run docker container as a
regular user. In the examples below, I am using Ubuntu 17.04, but the commands
should be the same for any Linux distribution.

## Add user to docker group

First of all, what happens if you try to run docker without sufficient
privileges? You can see the output below:

```text
$ docker run hello-world
docker: Got permission denied while trying to connect to the Docker daemon
socket at unix:///var/run/docker.sock: Post https://%2Fvar%2Frun%2Fdocker.sock/v1.28/containers/create: dial unix /var/run/docker.sock: connect: permission denied.
See 'docker run --help'.
```

Docker allows users in docker group to run containers. You
can check which users the group contains by running:

```bash
$ cat /etc/group | grep docker docker:x:140:
```

In the example above, the
group does not include any users. Therefore, you need root privileges to run
containers, as shown by the error above. This is the default setting. To add
your user to the docker group, run this command:

```bash
sudo usermod -a -G docker user
```
<!-- TEASER_END -->

## Reload the shell

Expanding the group is as easy as running this one command. But if you try
running docker in the same shell, you will still get the permission error. Why
is that? For the new groups to be loaded, you have to re-log:

```text
$ su user
```
Now, you can confirm, that you can run a docker container as a
regular user:

```text

$ docker run hello-world
Hello from Docker! This message shows that your installation appears
to be working correctly.
```

That's it! Enjoy your slightly safer docker! If you have
any questions, post them in the comments below.

<!--
.. title: Attack on the Titan Board Game
.. slug: attack-on-the-titan-board-game
.. date: 2016-10-18 13:25:42
.. tags: anime,antoine bauza,attack on the titan,board games,manga
.. category: Games
.. link: 
.. description: 
.. type: text
.. categories: Games
.. has_math: no
.. status: published
.. wp-status: publish
-->

Last weekend Spiel 2016, the biggest board game trade fair, took place in
Essen, Germany. While there, I had a chance to catch up with [Antoine
Bauza](https://twitter.com/Toinito), designer of such gems as
[Tokaido](https://boardgamegeek.com/boardgame/123540/tokaido), [7
Wonders](https://www.boardgamegeek.com/boardgame/68448/7-wonders) and [Samurai
Spirit](https://boardgamegeek.com/boardgame/158900/samurai-spirit).
Currently, he is working on Attack on the Titan: The Last Stand. The game
based is on the hugely popular Japanese manga and anime, where the remnants of
humanity live in a wall-protected city, defending themselves from titans, who
have an appetite for humans. In this board game, one player takes on the role
of a titan. Their goal is to eat innocent citizens, destroy the city's
defenses and kill the members of the defense corps. The other players team up,
trying to take the titan down.

![Attack on the Titan: The Last Stand](https://prodimage.images-bn.com/pimages/0814552021846_p1_v2_s550x406.jpg)

I got to try out the game prototype as well. The game-play was very immersive and action
packed, even though it was a bit too dice heavy for my taste. That was quite
surprising for me, since Bauza's games usually do not utilize dice at all. But
I will be definitely buying the full game once it comes out in February 2017
anyway.


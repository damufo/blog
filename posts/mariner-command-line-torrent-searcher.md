<!--
.. title: Mariner - a Command Line Torrent Searcher
.. slug: mariner-command-line-torrent-searcher
.. date: 2018-02-25 17:50:13
.. tags: application,command line,download,python,torrents
.. category: Linux
.. link: 
.. description: 
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

I have been busy working on a new project for the past several weeks. It has
finally reached a state, where I can publicly announce it. So without further
ado, welcome Mariner, the command line torrent searcher:

![Mariner in action](https://gitlab.com/radek-sprta/mariner/-/raw/master/docs/assets/mariner.svg)

I use torrents for downloading Linux distributions. The reason being, that it is
usually faster and easier than digging for the ISOs on the official websites.
Still, most torrent trackers are bloated, filled with ads and plagued by popup
windows. I could do without those. All this has lead me to write Mariner. As
you can see in the animation above, it is a simple command line aplication. It
let's you search for a torrent on multiple trackers at once and find the
fastest one. Among other features, it allows you to see the torrent details,
download them and directly open them in your torrent application. Moreover, it
is asynchronous. That means, it is more responsive than your average web
scraper.

## Conclusion

If it piqued your interest, you can learn more about Mariner in
[documentation](https://radek-sprta.gitlab.io/mariner/). Or, you could just
check the [project page](https://gitlab.com/radek-sprta/mariner). But if you
are looking for a more automatic way to download torrents, check my
[torrentbox guide](https://radeksprta.eu/turn-raspberry-pi-into-torrentbox/).


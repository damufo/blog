<!--
.. title: Use Caddy Reverse Proxy for Kibana
.. slug: use-caddy-reverse-proxy-for-kibana
.. date: 2020-02-07 20:48:15
.. tags: caddy,configuration,elastic,kibana,nginx,reverse proxy,tutorial,web server
.. category: Linux
.. link:
.. description:
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

[Nginx](https://www.nginx.com/) is probably the most widely used reverse proxy
software out there. But when it comes to Docker, I have started to favor
[Caddy](https://caddyserver.com/) over it. Caddy is a lightweight web server
written in Go. Among its advantages are extremely simple configuration and
support for automatic [Let's Encrypt](https://letsencrypt.org/) certificates.
Certainly the automatic HTTPS simplifies any Docker setup. While it is not yet
included in repositories and therefore lacks automatic updates, Docker
nullifies this drawback. So, I will show you how to setup Caddy reverse proxy
for Kibana.
<!-- TEASER_END -->

## Configuration example

You will find configuration for Caddy stored in `/etc/Caddyfile`. For
instance, it might look like this:

```text
example.com {
  basicauth / secret_user secret_password
  proxy /kibana 127.0.0.1:5601 {
    transparent
    without /kibana
  }
}
```

As you can see, the syntax is very concise. Now, let's go through the various options:

* `basicauth` - protect the site with username and password
* `proxy` - redirect all `/kibana` requests to port 5601
* `transparent` - add necessary header, such a X-Forwarded-For and X-Forwarded-Scheme
* `without` - strip `/kibana` from forwarded requests

## Conclusion

In just a couple of lines, you have setup Caddy reverse proxy for Kibana.
Complete with HTTPS and automatic certificates. That was really easy, right?
Now you understand, why Caddy is quickly becoming my favorite proxy. While
you're at it, try these [tips to increase Wordpress
performance](https://radeksprta.eu/improve-wordpress-load-speed-wp-super-cache/).

<!--
.. title: Fix Steam Controller Issues in Linux
.. slug: fix-steam-controller-issues-linux
.. date: 2016-12-25 09:05:17
.. tags: fix,steam,steam controller,update
.. category: Games
.. link:
.. description:
.. type: text
.. categories: Games,Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

Last time I was praising Steam Controller [for running smoothly in
Ubuntu](https://radeksprta.eu/using-steam-controller-in-ubuntu/). Well, I must
have jinxed it. As you might have noticed, after a recent update, it stopped
working all together. The reason is, Valve have changed the protocol for
wireless communication between the controller and the computer. Luckily, the
fix is easy. You just have to edit the udev rules. So, open them with a text
editor:

```text
$ sudo nano /lib/udev/rules.d/99-steam-controller-perms.rules
```

and change them to the following:

```bash
# This rule is needed for basic functionality of the controller in Steam and
# keyboard/mouse emulation
SUBSYSTEM=="usb", ATTRS{idVendor}=="28de", MODE="0666"

# This rule is necessary for gamepad emulation; make sure you
# replace 'pgriffais' with a group that the user that runs Steam belongs to
KERNEL=="uinput", MODE="0660", GROUP="pgriffais", OPTIONS+="static_node=uinput"

# Valve HID devices over USB hidraw
KERNEL=="hidraw*", ATTRS{idVendor}=="28de", MODE="0666"

# Valve HID devices over bluetooth hidraw
KERNEL=="hidraw*", KERNELS=="*28DE:*", MODE="0666"

# DualShock 4 over USB hidraw
KERNEL=="hidraw*", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="05c4", MODE="0666"

# DualShock 4 wireless adapter over USB hidraw
KERNEL=="hidraw*", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="0ba0", MODE="0666"

# DualShock 4 Slim over USB hidraw
KERNEL=="hidraw*", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="09cc", MODE="0666"

# DualShock 4 over bluetooth hidraw
KERNEL=="hidraw*", KERNELS=="*054C:05C4*", MODE="0666"

# DualShock 4 Slim over bluetooth hidraw
KERNEL=="hidraw*", KERNELS=="*054C:09CC*", MODE="0666"
```

Now, save them with `Ctrl + x`.

I wonder, how much of a problem would be to update the udev package along the
Steam update. While the updated rules were mentioned in the change log, it did
not say which file you were supposed to edit. And of course, not everybody
reads changelogs. So this caused a headache to lot of users. Hopefully, Valve
will handle updates better in the future.

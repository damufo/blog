<!--
.. title: Automatically Turn Off the LEDs on Raspberry Pi
.. slug: automatically-turn-off-the-leds-on-raspberry-pi
.. date: 2016-10-10 11:19:08
.. tags: bash,LED,script,sleep
.. category: Raspberry Pi
.. link: 
.. description: 
.. type: text
.. categories: Raspberry Pi
.. has_math: no
.. status: published
.. wp-status: publish
-->

Raspberry Pi is my little busy-bee, working as a
[torrentbox](https://radeksprta.eu/turn-raspberry-pi-into-torrentbox/) and a
[Nextcloud server](https://radeksprta.eu/installing-nextcloud-on-raspberry-pi/),
among other things. But man, those [blinking LEDs are not good for your
sleep](https://chriskresser.com/how-artificial-light-is-wrecking-your-sleep-and-what-to-do-about-it/).
So, I have written a small script with a systemd
service to keep the LEDs turned off at night. You can find it at my [Gitlab
page](https://gitlab.com/radek-sprta/automate-raspberry-leds). Enabling it on
your own Raspberry is as easy as 1-2-3:

1. First, download the script with
`git clone https://gitlab.com/radek-sprta/automate-raspberry-leds` (you might
have to install `git` first).
2. Then copy `raspberry-leds.sh` to `/usr/local/bin`.
3. And make it executable with `sudo chmod +x /usr/local/bin/raspberry-leds.sh`.
4. Afterwards, `raspberry-leds.service` and `raspberry-leds.timer` to `/etc/systemd/system/multi-user.target.wants`.
5.  Finally, run `sudo systemctl daemon-reload`.

Well, that was more like
1-2-3-4-5. But still pretty easy I would say. No more blinking lights
bothering you at night anymore. Yay!

<!--
.. title: Website Obesity
.. slug: website-obesity
.. date: 2017-03-04 16:01:45
.. tags: bloat,internet,Link,talk,website
.. category: Essay
.. link: 
.. description: 
.. type: text
.. categories: Essay
.. has_math: no
.. status: published
.. wp-status: publish
-->

The other day, I stumbled upon an interesting talk by Maciej Cegłowski about
the worrying trend of website obesity. I have to say, I fully agree with him.
As it stands, the situation is getting quite ridiculous. Designers and web
coders should stop the unnecessary bloat. Here's the transcript of the talk:

[Website Obesity](https://idlewords.com/talks/website_obesity.htm)

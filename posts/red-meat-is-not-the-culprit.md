<!--
.. title: Red Meat Is Not the Culprit
.. slug: red-meat-is-not-the-culprit
.. date: 2019-03-17 17:19:59
.. tags: diet,environment,podcast,Link,red meat
.. category: Health
.. link: 
.. description: 
.. type: text
.. categories: Health
.. has_math: no
.. status: published
.. wp-status: publish
-->

Nowadays, eating red meat is more and more stigmatized. Not only do the media
portray it as unhealthy, but lately even as the biggest factor behind
environmental issues. [EAT-Lancet
paper](https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(18)31788-4/fulltext)
is just the latest in the series of such alarmist articles. As you can see
from the outline, it recommends a diet based on highly processed plant-based
foods. No surprise, considering that processed food giants, such as Nestle and
Kellogg's, sponsored it. But fortunately, numerous people have already rebuked
the misconceptions, that the paper mentions. Personally, I have liked this
Revolution Health Radio interview with Diana Rodgers. You can find it here:

[What the EAT-Lancet Paper Gets Wrong, with Diana
Rodgers](https://chriskresser.com/what-the-eat-lancet-paper-gets-wrong-with-diana-rodgers/)

If you find it intriguing, I recommend [Impacts and Ethics of
Eating Meat](https://radeksprta.eu/impacts-and-ethics-of-eating-meat/) as well.


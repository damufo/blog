<!--
.. title: Powerfish Gets New Features and Support for Themes
.. slug: powerfish-gets-new-features-support-themes
.. date: 2017-07-20 18:32:37
.. tags: fish,fisherman,git,powerline,prompt
.. category: Linux
.. link: 
.. description: 
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

I dedicated couple evenings to Powerfish lately, resulting in a host of new
features. If you don't know what I'm talking about, [Powerfish is an elegant
and informative prompt for the Fish shell inspired by
Powerline](https://radeksprta.eu/powerfish-an-elegant-and-informative-prompt/).
Without further ado, let's see what is new.

## New features

First of all, Powerfish now fully supports the Vi keybindings. So there is no
ugly box showing the current mode at the beginning of the prompt anymore. A
small change, but definitely visible, if you use the Vi mode, like me. There
are also two new flags in the prompt. One displays the number of background
jobs, and another one shows if the last command failed. The Git flags were
overhauled as well. Now they show number of untracked, modified, staged,
stashed and conflicting files. If you don't care about the concrete numbers,
there is an option to display only the flags.
<!-- TEASER_END -->

## Support for themes

Last, but not least - Powerfish now supports themes. That makes it to
seamlessly blend in with the rest of your system. The ones currently supported
are Tomorrow Night and Solarized Dark, but more are on the way.

![Powerfish with Tomorrow Night theme](https://gitlab.com/radek-sprta/powerfish/raw/master/prompt-tomorrow-night.png)

## Easier installation

If you have already have [Fisherman](https://github.com/fisherman/fisherman),
just type `fisher radek-sprta/powerfish` to install Powerfish. Otherwise,
follow the instructions on the [project page](https://gitlab.com/radek-sprta/powerfish).


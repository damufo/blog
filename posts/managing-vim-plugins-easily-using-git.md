<!--
.. title: Managing Vim Plugins Easily Using Git
.. slug: managing-vim-plugins-easily-using-git
.. date: 2017-08-05 23:21:52
.. tags: configuration,dotfiles,git,pathogen,plugins,submodules,vim,vimrc
.. category: Linux
.. link:
.. description:
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

Managing Vim plugins can quickly get messy. Fortunately, there are several
solutions to this problem. My favorite one is
[Pathogen](https://github.com/tpope/vim-pathogen) by Tim Pope. It makes it
easy to install plugins in their own private directories and automatically
load them. And combining it with Git will make your Vim configuration easily
portable across computers.

## Install Pathogen

So how do we set it up? First of all, prepare a Git repository for your Vim
plugins:

```text
$ git init .vim
$ mkdir .vim/bundle
```

Next, get Pathogen. To makes things easier, we will add it as Git submodule:

```text
$ git submodule add https://github.com/tpope/vim-pathogen bundle/pathogen
$ ln -s autoload bundle/pathogen/autoload
```

Furthermore, to enable Pathogen, you must add the following lines to your
`.vimrc`:

```vim
execute pathogen#infect() syntax on filetype plugin indent on
```

## Managing Vim plugins

Now you can happily start adding other plugins as submodules. Here is an
example using [Vim Sensible](https://github.com/tpope/vim-sensible):

```text
$ git submodule add https://github.com/tpope/vim-sensible bundle/vim-sensible
```
 In order to update them later, just run:

```text
$ git submodule update --remote --merge
```

Finally, if you ever need to delete them later, follow these steps:

```bash
$ git submodule init bundle/vim-sensible
$ git rm bundle/vim-sensible
$ rm -Rf .git/modules/bundle/vim-airline
$ git commit
```

## Synchronize everything

Just one thing is missing to make the setup perfect. Ideally, your vimrc would
be stored along with the plugins. I prefer
[dotfiles](https://pypi.python.org/pypi/dotfiles/) for the job. For it is
simple and ready to install using pip. So let's do that:

```text
$ sudo pip install dotfiles
```

Then create a folder for your Vim configuration
(which you can use for other configurations as well):

```text
$ git init Dotfiles
```

Finally, add the Vim configuration files:

```bash
$ dotfiles --add ~/.vimrc
$ dotfiles --add ~/.vim
```

 Voilà. At last, you can keep the changes to your Vim
settings and plugins safely versioned now. And to set them up on a new
computer, you just need to clone the repository, install dotfiles and run:

```text
$ dotfiles --sync
```

Can't get easier than that. Now, head over to see [some of my Vim
tips](https://radeksprta.eu/vim-tips-increased-productivity).

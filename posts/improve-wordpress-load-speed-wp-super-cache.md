<!--
.. title: Improve Wordpress Load Speed with WP Super Cache
.. slug: improve-wordpress-load-speed-wp-super-cache
.. date: 2018-05-09 17:12:37
.. tags: cache,configuration,performance,wordpress,wp super cache
.. category: Linux
.. link: 
.. description: 
.. type: text
.. categories: Unsorted
.. has_math: no
.. status: published
.. wp-status: publish
-->

Wordpress is the most popular CMS in the world, powering [over 30% of all
websites](https://w3techs.com/). It owes its popularity to easy of use and a
huge number of plugins. The plugins make it possible to turn Wordpress into
anything from personal blog a to e-shop. Like others of its kind, Wordpress
generates the pages dynamically. In other words, everytime it is accessed, it
loads data from database and uses PHP to create a page it serves the visitor.
As a result, Wordpress load speed is slower a it requires more resources. That
is important point too keep in mind, since over [40% of users leave a page if
does not load within 3 seconds](https://blog.kissmetrics.com/loading-time/?wide=1).

## Caching

This is where caching comes into picture. It combines the speed of static
sites with the robustness of Wordpress. Instead of generating page on every
load, Wordpress saves the resulting page. Other visitors just see this
pregenerated static page. This significantly improves Wordpress load speed and
saves server resources. And when you post a new article or a comment, the
cache is automatically invalidated. So, you don't have to be afraid, that
visitor will see an outdated version.
<!-- TEASER_END -->

## Installing WP Super Cache

There are several caching plugins for Wordpress. [WP Super
Cache](https://wordpress.org/plugins/wp-super-cache/) is the most popular one.
So, that is the one we will choose. The installation is straightforward, just
login into the Wordpress dashboard and go to Plugins → Add new.

## Settings

To get the most out of WP Super Cache, it is important to fine tune it to your
website's needs. You can find the options in Settings → WP Super Cache. The
basic view looks as following:

![Basic WP Super Cache settings](https://radeksprta.eu/wp-content/uploads/2018/05/1.png)

On top, there is a list of tabs: Easy, Advanced, CDN, Contents, Preload,
Plugins, Debug. Only the first three are interesting for us. First of all,
check the **Caching on** checkbox to turn on the plugin on the Easy tab. When
done, you can run the **Cache Tester** and see if everything works fine.

## Advanced

This is where all the important options are. As you can see on the following
screenshot, there is quite a few of them. Let's take a look at what they mean:

![Advanced WP Super Cache settingsAdvanced WP Super Cache settings](https://radeksprta.eu/wp-content/uploads/2018/05/2.png)

- **Cache Delivery Method**. You have two options: 
  - **Simple** – cached files are
served via PHP. The more compatible of the two.
  - **Expert** – it is more
performant, but requires Apache's .htaccess and mod_rewrite to work.
- **Don’t cache pages for known users.** As in, users that have logged in, have the author cookie or left a comment on the website. I recommend to turn this on.
- **Don’t cache pages with GET parameters.** For example, search results. You
can leave this on.
- **Compress pages, so they’re served more quickly to
visitors.** This speeds up loading, but it causes problems for people with
older browsers. Therefore, I recommend to leave this off.
- **Cache rebuild. Serve a supercache file to anonymous users while a new file is being
generated.** It prevents generating a page multiple times at once.
Recommended.
- **304 Not Modified browser caching. Indicate when a page has not
been modified since it was last requested.** This tells browser to use its own
cache. However, some older browsers don't handle the return code properly, so
it's best to leave it off for now.
- **Make known users anonymous so they’re
served supercached static files.** This causes situations, where user does not
see a comment they've just posted a may resubmit it several times. Therefore,
I avoid using it.
- **Enable dynamic caching.** This requires changes in the
source code.
- **Mobile device support.** Requires WP Touch plugins. Consider
this only if your theme is not responsive. **Remove UTF8/blog charset support
from .htaccess file.** Turn this on, if you see strange characters on cached
pages.
- **Clear all cache files when a post or page is published or updated.**
You can usually leave this disabled.
- **Extra homepage checks.** If the content of the main page changes often, it might be a good idea to enable this.
- **Only refresh current page when comments made.** You can safely leave this turned
off.
- **Coarse file locking.** Prevents duplicate cache files, but has a
negative impact on performance. Therefore, I advise to leave it off.
- **Cache Timeout** sets how long should be the pages cached. The optimal value depends
on how frequently the content changes. It is a good idea to start with one
hour and adjust it from there. However, don't set the timeout too long (over 1
day). Set the timer to a fraction of cache timeout. There is no need to meddle
with the rest of the settings on this tab.

## CDN

CDN is a technology for faster delivery of static content. It speeds up page
loads by using caching servers in multiple locations and thus shortening the
distance between the visitor and the web server. If you are using a CDN, you
can set up WP Super Cache to correctly work with it. Enter your website's
domain into **Site URL**. Then, put the **CDN URL** to Off-site URL field. If
you use multiple ones, you can enter them to **Additional URLs**. As for the
**include directories** and **exclude directories** , you don't need to touch
them.

## Conclusion

This wraps up the settings. Now, you should have WP Super Cache tuned up for
maximum performance and reliability. You can check the load times using
<https://performance.sucuri.net>. For other configuration tips, check
<https://radeksprta.eu/categories/configuration/>.

<!--
.. title: How to Connect External Hard Drive to Raspberry Pi
.. slug: how-to-connect-external-hard-drive-to-raspberry-pi
.. date: 2018-12-18 00:02:52
.. updated: 2020-11-04 00:25:52
.. tags: configuration,external drive,tutorial,usb
.. category: Raspberry Pi
.. link:
.. description:
.. type: text
.. categories: Raspberry Pi
.. has_math: no
.. status: published
.. wp-status: publish
-->

I have already explained how to do a basic [Raspberry Pi server
install](https://radeksprta.eu/setting-up-headless-raspberry-pi-server/). But
if you want to do something more exciting with it, perhaps [running a
Nextcloud instance](https://radeksprta.eu/installing-nextcloud-on-raspberry-pi/)
or [setting up a torrent box](https://radeksprta.eu/turn-raspberry-pi-into-torrentbox/),
you will need more space than just an SD card. That's why
in this post I will show you how to connect external hard drive to Raspberry
Pi.
<!-- TEASER_END -->

~~If you don't already have an external hard drive, I recommend the
Western Digital Pi Drive,
which is designed to work with Raspberry Pi.~~ For other drives, you must
increase how much juice Raspberry puts out via its USBs, because the default
is not enough for an external drive. So, open `/boot/config.txt` in `nano` and
add this line:

```text
max_usb_current=1
```

Save the file with
`Ctrl+X` and restart your Pi. Now, connect the drive and try to mount it to
see if it works. To find out its device name, use `sudo blkid`. It should
probably be something like `/dev/sda1`. The mount command is easy:

```text
$ mount /dev/sda1 /mnt
```

Next, try
saving a test file to the drive. If it's saved successfully, you are good to
go. If you get any errors, your drive is probably too power hungry and you
might need to connect it to your Pi through an externally powered USB hub.

## Automatic mounting

Mounting the drive manually every time you restart your Pi is a bit of pain
though. So let us make it mount automatically. First, you need to make a mount
point for it:

```text
$ mkdir /media/external
```

Of course, you can call whatever you want. When
done, edit the `/etc/fstab` file, adding the following line:

```text
/dev/sda1 /media/external ntfs defaults 0 0
```

Instead of
`/dev/sda1`, put in your own device name. This assumes the drive uses the
default Windows filesystem - ntfs. If you use something else instead (ext4,
btrfs), change it accordingly. And that's it - you have successfully connected
an external drive to your Raspberry Pi.

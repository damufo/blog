<!--
.. title: Setting Up Headless Raspberry Pi Server
.. slug: setting-up-headless-raspberry-pi-server
.. date: 2016-04-22 18:59:48
.. tags: automatic updates,command line,raspberry pi,raspbian,security,server,ssh,tutorial,wifi
.. category: Raspberry Pi
.. link:
.. description:
.. type: text
.. categories: Raspberry Pi
.. has_math: no
.. status: published
.. wp-status: publish
-->

The Raspberry Foundation has recently launched [Raspberry Pi
3](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/), a new
version of their popular developer board. It features a faster ARM processor,
1 GB of RAM and, most importantly, on-board wifi. This makes it even better
choice for a headless Linux server. This guide shows how to do the basic
Raspberry Pi server setup, with password-less SSH and automatic updates.

## Get the Raspbian Lite image

First of all, you have to download a Linux image for your Raspberry Pi. For
using it as a headless server I recommend the Raspbian Lite, which is a
minimal image shipped without the desktop packages (which are obviously not
needed for a headless server). You can download it
[here](https://downloads.raspberrypi.org/raspbian_lite_latest). When the
download is complete, open the `.zip` file and unzip the `.img` file
containing the image.
<!-- TEASER_END -->

## Write the image to SD card

Now, with the image ready, you can flip out your SD card. First of all, run
`df -H` command to see what filesystems are currently mounted. Insert your SD
card and `df -H` again. It should show up as something along the lines of
`/dev/sdb1` or `/dev/mmcblk0p1`. Take note of the device path. Before
proceeding further, you have to unmount all the partitions of the SD card
(sdb1, sdb2 etc.). The command for `/dev/sdb1` partition would be:

```text
$ sudo umount /dev/sdb1
```

With all the partitions of the SD card unmounted, you can proceed with writing the
image on it. For that, use the `dd` command. `dd` takes two arguments - `if=`,
which specifies the input file, and `of=`, which specifies the output file.
`if=` would be the complete path to your image file and `of=` the path to your
SD card. You must use the device path of the whole SD card, not just the
partitions. If your partition's name is `/dev/sdb1`, then the device path
would be `/dev/sdb`. In case of `/dev/mmcblk0p1,` it would be `/dev/mmcblk0`.
Make sure to get the `if=` and `of=` parts right, as you could potentially
compromise your whole system. The example command is:

```text
$ sudo dd bs=4M status=progress if=2016-03-18-raspbian-jessie-lite.img of=/dev/sdb
```
The command will take couple minutes to
complete, but it will keep you periodically updated about the progress.

## Connect to your Raspberry Pi

The recent versions of Raspbian have the ssh server turned off by default. To
turn it on, create a file called `ssh` in the boot partition.

```text
$ sudo mount /dev/sdb1/ /mnt
$ cd /mnt
$ touch ssh
```

When finished, use the `sync` command to make sure that
the cache is flushed, take out the SD card and put it in your Raspberry Pi.
Plug in the ethernet cable, power it on and you can happily use `ssh` to
connect to it by using the hostname and the default password `raspberry`:

```bash
$ ssh pi@raspberrypi.local
```

If somehow this does not work,
just scan your network for devices with open port 22 (the ssh port). First of
all, check your ip address with `ip addr`. It should be something like
`192.168.0.100/24`. In that case you would use the following command to scan
the network:

```text
$ nmap -p 22 192.168.0.1/24
```

One of the
devices with open port 22 (or, perhaps, the only such device) is your
Raspberry Pi. Let's say its ip address is `192.168.0.105`. You can login with
using the following command and the password `raspberry`:

```bash
$ ssh pi@192.168.0.105
```

## Basic configuration

Congratulations, you should be logged in to your Raspberry Pi! Now it is time
for some basic configuration. First of all, expand the file system, so that
your Pi can use the whole SD card. You can use the `raspi-config` utility for
that:

```text
$ sudo raspi-config
```

From there, go to `Advanced Options` and choose the first
option. While at it, change the `Memory Split` to 0. You do not need a graphic
memory on your headless server, so you might as well use it for something
useful.

## Set up automatic updates

Last, but not least, you should set up automatic updates, so you get the
latest security patches. First, let's do a regular update by the following
command:

```bash
$ sudo apt update && sudo apt upgrade -y
```

This might take a while. Next, install the
`unattended-upgrades` package:

```text
$ sudo apt install unattended-upgrades
```

Edit its config using the command line:

```bash
$ sudo nano /etc/apt/apt.conf.d/50unattended-upgrades
```

and uncomment these lines:

```text
"o=Raspbian,n=buster"; "o=Raspbian,n=stable";
Unattended-Upgrade::Remove-Unused-Dependencies "true";
```
If you do not mind
having your server reboot for the updates that require it, uncomment the
following lines as well:

```text
Unattended-Upgrade::Automatic-Reboot "true";
Unattended-Upgrade::Automatic-Reboot-Time "02:00";
```

## Where to go next?

Since the post was getting a bit long, I have split off the other sections to
separate articles:

  1. [Configure the wifi on Raspberry Pi](https://radeksprta.eu/posts/raspberry-pi-wifi-setup/)
  2. [Set up password-less SSH login and secure your SSH](https://radeksprta.eu/posts/set-up-password-less-ssh-login-and-secure-your-ssh/)
  3. [Change the username, hostname and password for Raspberry Pi](https://radeksprta.eu/posts/change-raspberry-pi-password-username-hostname/)
  4. [Assign your Raspberry Pi static IP](https://radeksprta.eu/posts/assign-raspberry-pi-static-ip-couple-steps/)
  5. [How to connect external hard drive to Raspberry Pi](https://radeksprta.eu/posts/how-to-connect-external-hard-drive-to-raspberry-pi/)

You can also check how to set up specific services:

  1. [Turn Raspberry Pi Into Torrentbox](https://radeksprta.eu/posts/turn-raspberry-pi-into-torrentbox/)
  2. [Install Nextcloud on Raspberry Pi](https://radeksprta.eu/posts/installing-nextcloud-on-raspberry-pi/)
  3. [Blocking Ads with Raspberry Pi](https://radeksprta.eu/posts/use-pi-hole-block-ads-raspberry-pi/)
  4. [Automatically turn off LEDs on Raspberry](https://radeksprta.eu/posts/automatically-turn-off-the-leds-on-raspberry-pi/)
  5. [Setup Mosh](https://radeksprta.eu/posts/setup-mosh-sshs-cousin-for-mobile-era/)

Set up anything you fancy and enjoy your Raspberry Pi!

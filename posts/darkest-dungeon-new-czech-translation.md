<!--
.. title: Darkest Dungeon on PS4/PS Vita, Now With New Czech Translation
.. slug: darkest-dungeon-new-czech-translation
.. date: 2016-09-28 18:04:12
.. tags: darkest dungeon,ps vita,ps4,release,translation
.. category: Games
.. link: 
.. description: 
.. type: text
.. categories: Games
.. has_math: no
.. status: published
.. wp-status: publish
-->

Darkest Dungeon is probably my favorite game released this year. It is a
Gothic rogue-like about the stresses of dungeon crawling. You lead a party of
heroes trying to stop an ancient evil encroaching upon your birth village. But
you do not stand only against creatures of the dark. Hunger, diseases and
unimaginable horrors that your heroes encounter will all take a toll on their
mental state. They will despair, they will go mad, they will die... and they
won't return. The game features challenging tactical combat and gloomy
Lovecraftian hand-drawn graphics.  Yesterday, the Red Hook released the long-
awaited PS4/PS Vita version. I am personally invested in this one, because
supposed features a remade Czech localization. The original localization was
plagued by inconsistent, too literal, and sometimes outright wrong
translations. I took upon the task of bringing it up to standards. And many
sleepless nights later, the Czech gamers should have a much more pleasant
experience. So, for those of you who have not tried the game yet, this is the
perfect chance. If you are still not convinced, take a look at the is a
release trailer:

{{% media url="https://youtu.be/Ng7Ctx50ixs" %}}

Edit: And now there is a [DLC on
the way too](https://radeksprta.eu/darkest-dungeon-the-crimson-court-dlc-announced/)!


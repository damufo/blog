<!--
.. title: Use Pi-Hole to Block Ads with Raspberry Pi
.. slug: use-pi-hole-block-ads-raspberry-pi
.. date: 2016-10-27 01:39:45
.. tags: ad blocking,ads,pi-hole,privacy,server,tutorial
.. category: Raspberry Pi
.. link:
.. description:
.. type: text
.. categories: Raspberry Pi
.. has_math: no
.. status: published
.. wp-status: publish
-->

We live surrounded by ads. Not only in the physical space, but also in the
virtual reality of Internet. They don't just consume a lot of bandwidth and
are very obstructive, they usually also unethically track you. While I
understand, that content creators need the means to make ends meet, I believe
there are better ways to accomplish that, such as
[Patreon](https://www.patreon.com/).  Now, there are plenty of ways to get rid
of ads on traditional computers. For example, browser extensions, such as
[Privacy Badger](https://www.eff.org/privacybadger) and [uBlock
Origin](https://www.ublock.org/), or proxys, such as
[Privoxy](https://www.privoxy.org/)). However, on mobile devices, the
situation is far less ideal. One way to tackle the problem, is to filter out
ads for your whole network. Welcome to [Pi-Hole](https://pi-hole.net/). It is
a DNS server for Raspberry Pi, that keeps a blacklist of advertising sites and
serves an empty response instead of them, saving both your bandwidth and your
sanity.

## Installation

As you can see on its website, Pi-Hole's installation process is very easy.
Once you have your [Raspberry Pi set up](https://radeksprta.eu/setting-up-headless-raspberry-pi-server/),
connect to it via SSH. Then run the following
command to download Pi-Hole:
<!-- TEASER_END -->

```bash
$ curl -L https://install.pi-hole.net | bash
```

Next, open the necessary ports if you are using `ufw`:

```text
$ sudo ufw allow dns
$ sudo ufw allow http
```

Take a note of your Pi's
IP address:

```text
$ ip addr
```

Lastly, open the settings of your router and change the DNS address to the
address of your Pi.

## Conclusion

You are done. No more ads on any of the devices connected to your network. And
if you want to see how many ads has Pi-Hole blocked, just type your
pi.hole/admin into your browser and smile (and optionally whitelist websites,
that are sensible about the ads they show).

![Pi-Hole Dashboard](https://pi-hole.net/wp-content/uploads/2016/06/dashboard-no-berry.png)

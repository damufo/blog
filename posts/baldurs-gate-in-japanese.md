<!--
.. title: Baldur's Gate in Japanese
.. slug: baldurs-gate-in-japanese
.. date: 2015-05-26 15:59:52
.. tags: language learning,rpg
.. category: Games
.. link:
.. description:
.. type: text
.. categories: Games,Japanese
.. has_math: no
.. status: published
.. wp-status: publish
-->

The other day I saw [Baldur's Gate: Enhanced Edition
](https://www.beamdog.com/games/baldurs-gate-enhanced/) on sale on
Steam. Without a second thought, I decided to check it out. After all, it was
one of my favourite games. To my great surprise, when I turned the game on, it
was all in Japanese! I always assumed there was some kind of region
restriction on Steam, because no other game I own ever came with Japanese
translation.

![Baldur's Gate in Japanese](https://radeksprta.eu/wp-content/uploads/2015/05/2015-05-23_00001-300x168.jpg)

The developers did a fine job bringing the game closer to today's standards.
The new characters are interesting and the quick loot function is something I
could not live without again. And while a lot of the community mods are not
compatible with the Enhanced Edition, the number of the supported ones grows
by the day. Learning Japanese while playing one of the best RPGs of all time -
does it not sound great? Try Baldur's Gate in Japanese today! And if you are
still hesitant, there is even a [new expansion in the
works](https://radeksprta.eu/baldurs-gate-expansion/) to lure you. So what are
you waiting for? ;)

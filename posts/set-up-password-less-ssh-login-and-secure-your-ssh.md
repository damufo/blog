<!--
.. title: Set Up Password-less SSH Login and Secure Your SSH
.. slug: set-up-password-less-ssh-login-and-secure-your-ssh
.. date: 2017-06-26 16:12:57
.. tags: rapsbian,security,server,ssh,tutorial
.. category: Raspberry Pi
.. link:
.. description:
.. type: text
.. categories: Raspberry Pi
.. has_math: no
.. status: published
.. wp-status: publish
-->

You have Raspberry Pi [set up and running](https://radeksprta.eu/setting-up-headless-raspberry-pi-server/),
but typing password every time you try to
login using ssh is quite the pain. Well, using key-based authentication is not
only more convenient, but also more secure. Let's see how to set it up.

## Set up password-less SSH

First of all, you need to generate the public / private key for your computer
using this command:

```text
$ ssh-keygen -t rsa -b 4096
```

You will
be asked for a pass-phrase. Using one is highly recommended. You should use
one that is long, preferably a whole sentence, and different from your
password. Avoid using famous quotes, as your pass-phrase could easily get
cracked by a dictionary attack. Next, upload the key to your Pi:

```bash
$ ssh-copy-id -i ~/.ssh/id_rsa.pub pi@raspberrypi.local
```
If it failed
to connect, just use the Pi's ip adress in place of `rapsberrypi.local`. You
should now be able to connect to your pi using the key.
<!-- TEASER_END -->

## Securing SSH

You should take at least some basic security measures to prevent other people
from doing nasty stuff to your Raspberry. So open `/etc/ssh/sshd_config` with:

```text
$ sudo nano /etc/ssh/sshd_config
```

And uncomment the following line:

```text
AuthorizedKeysFile %h/.ssh/authorized_keys
```

While you are
at it, you can also disable root login, login with password and change the
default port to make your server more secure. Change the `PermitRootLogin` and
`PasswordAuthentication` to `no`:

```text
PermitRootLogin no
PasswordAuthentication no
```

and change the port to your favorite
number between `1024` and `65535`, let's say `1337`:

```text
Port 1337
```

Save the configuration using `ctrl + x` and you are done. Now
you can connect to your Pi using your new port:

```bash
$ ssh -p 1337 pi@raspberrypi.local
```

And while in there, change the permissions on
the keys, so other users cannot read them. Just to be on the safe side.

```bash
$ chmod 700 ~/.ssh && chmod 600 ~/.ssh/authorized_keys.
```

That is everything. Now, that you have set
up password-less SSH, your Raspberry is more convenient to use and also more
resilient to random people trying to get inside.

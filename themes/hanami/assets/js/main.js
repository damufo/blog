// Set the correct mode on page load
document.addEventListener('DOMContentLoaded', (event) => {loadMode()});

function getPreferredMode() {
    // Set default mode to dark, if user prefers it, otherwise light
    var defaultMode = prefersDark() ? 'dark' : 'light';
    return localStorage.getItem('mode') || defaultMode;
}

function loadMode() {
    var storedMode = localStorage.getItem('mode');

    if (getPreferredMode() === 'dark') {
        document.body.classList.add('dark');
    } else {
        document.body.classList.remove('dark');
    }
}

function modeSwitch() {
    // Switch the page mode
    localStorage.setItem(
        'mode',
        getPreferredMode() === 'light' ? 'dark' : 'light');
    loadMode();
}

function prefersDark() {
    // Returns true, if user's browser prefers dark mode
    if (window.matchMedia &&
        window.matchMedia('(prefers-color-scheme: dark)').matches) {
        return true;
    }
    return false;
}

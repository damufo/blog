FROM python:latest AS builder

# Copy the whole repository into Docker container
COPY . . 

# Build the blog
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python \
    && apt update \
    && apt install -y jpegoptim \
    && $HOME/.poetry/bin/poetry install \
    && $HOME/.poetry/bin/poetry run nikola build


FROM nginx:alpine
MAINTAINER Radek Sprta <mail@radeksprta.eu>

EXPOSE 80

HEALTHCHECK --interval=10s --retries=3 --start-period=5s \
    CMD curl -f http://localhost/ || exit 1

# Copy output to the default nginx directory
COPY --from=builder output /usr/share/nginx/html

# Copy nginx host configuration
COPY nginx/default.conf /etc/nginx/conf.d/
